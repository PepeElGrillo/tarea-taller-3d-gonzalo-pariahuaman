﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace ConsoleApp1
{
    class Square : Shape
    {
        public int side;

        public Square(string name, int side) : base(name) {
            this.side = side;
        }

        public override void Area() {
            Debug.Log("Area = " + (side * side));
        }

    }
}
