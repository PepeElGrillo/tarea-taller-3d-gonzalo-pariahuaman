﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace ConsoleApp1
{
    class Circle : Shape
    {
        public int radius;

        public Circle(string name, int radius) : base(name) {
            this.radius = radius;
        }

        public override void Area() {
            Debug.Log("Area = " + (Math.PI*radius*radius));
        }
    }
}
